---
layout: markdown_page
title: "Why Ultimate/Gold?"
---

Ultimate is ideal for projects with executive visibility and strategic organizational usage. Ultimate enables enterprises transform IT by optimizing and accelerating delivery while managing priorities, security, risk, and compliance. Ultimate enables enterprises achieve advanced DevOps maturity by introducing enterprise capabilities to:

## Increase Operational Efficiencies
Ultimate enables enterprises maintain a consistent DevOps experience with a single, scalable interface for end to end DevOps. With this, developer satisfaction and productivity is massively improved with more time for value added projects rather than spending time learning different toolsets and integrating them. Additionally, DevOps Dashboards & Analytics available in Ultimate facilitate greater visibility and transparency across projects - helping to improve efficiencies and eliminate bottlenecks in the organization. Ultimate introduces Project Insights and Portfolio Management in the product to increase organizational efficiencies.
> **~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience** <br><br> Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts.<br> ***Zaq Wiedmann*** <br> Lead Software Engineer, Glympse <br> [Read more](/customers/glympse/)

## Deliver Better Products Faster
Ultimate enables seamless collaboration across Dev and Ops with continuous innovation. This continuous feedback loop from customers, deployments and monitoring operations back into development helps enterprises create products relevant for customers, go to market faster and beat competition as well. With every change undergoing rigorous testing, security & compliance testing, and incremental deployment - Ultimate allows rapid, iterative deployment of better quality software. Ultimate enhances configuration and monitoring to deliver products faster.
>  **Jenkins build took 3 hours, now with GitLab it takes 30 mins: a 6x improvement** <br><br>GitLab has allowed Alteryx to have code reviews, source control, continuous integration, and continuous deployment all tied together and speaking the same language. The team took a build that was running legacy systems and moved it to GitLab. This build took 3 hours on the Jenkins machine and it took 30 minutes to run on GitLab after it was going. Engineers can actually look at the build and understand what’s going on; they’re able to debug it and make it successful <br> [Read more](/customers/alteryx/)

## Reduce Security & Compliance Risk
Ultimate introduces capabilities to help reduce and manage risk from security and regulatory compliance by identifying vulnerabilities and compliance issues before the code ever leaves the developer's hands. Ultimate introduces Application Security Testing, Security Dashboards, and more advanced common controls and audit to reduce risks.
>  **BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use helping the team identify previously unidentified vulnerabilities**<br><br> One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role. <br> ***Adam Dehnel*** <br> *Product architect, BI Worldwide* <br> [Read more](/customers/bi_worldwide/)

Ultimate also includes priority support (4 business hour support), live upgrade assistance and a Technical Account Manager - who can aid you to achieve your strategic objectives and gain maximum value from your investment in GitLab.

Read all case studies [here](/customers/)

# Ultimate Specific Features

The below list of features are after factoring in the announcement regarding [18 GitLab features moving to core](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/). The timelines of the actual move of features to core in the product will be as per the linked issues in the announcement. 

#### Security

| Cybersecurity is a critical concern of every business leader.  Your applications MUST be secure. GitLab Ultimate weaves security into the pipeline to provide early and actionable feedback to the development team.  | [![Security Dashboards](/images/feature_page/screenshots/61-security-dashboard.png)](https://about.gitlab.com/images/feature_page/screenshots/61-security-dashboard.png) |

| Features    | Value |
| --------- | ------------ |
| [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) | Evaluates the static code, checking for potential security issues.   |
| [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/) | Analyzes the review application to identify potential security issues.  |
| [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) |  Evaluates the third-party dependencies to identify potential security issues.   |
| [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) |  Analyzes Docker images and checks for potential security issues.  |
| [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#project-security-dashboard) | Visualize the latest security status for each project and across projects. |
| [*Security Metrics and Trends* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6954)| *Metrics and historical data about how many vulnerabilities have been spotted, addressed, solved, and how much time was spent for the complete cycle.* |

#### Compliance

| The compliance tools provided by GitLab let you keep an eye on various aspects of your project. | ![License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/img/license_compliance_pipeline_tab_v13_0.png) |

| Features    | Value |
| --------- | ------------ |
| License Compliance |  Identify the presence of new software licenses included in your project and track project dependencies. Approve or deny the inclusion of a specific license. |
| Compliance Dashboard |  Compliance dashboard gives you the ability to see your group’s Merge Request activity by providing a high-level view for all projects in the group and approvers for the merge request |
| [*CD with SOC 2 Compliance*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/4120) | *Support SOC 2 compliance* |

#### Project and Group Insights

| Get insights that matter for your projects such as triage hygiene, issues created/closed per a given period, average time for merge requests to be merged, amongst others. | ![Project Insights](https://docs.gitlab.com/ee/user/project/insights/img/project_insights.png) |

| Features    | Value |
| --------- | ------------ |
| [Project Insights](https://docs.gitlab.com/ee/user/project/insights/) | Visualize project insights to improve developer efficiencies.   |


#### Portfolio Management

| Establish end to end visibility from Business Idea to delivering value. GitLab Ultimate, enables portfolio planning, tracking, and execution in one tool, that unifies the team to focus on delivering business value. | [![Epics](/images/feature_page/screenshots/51-epics.png)](https://about.gitlab.com/images/feature_page/screenshots/51-epics.png) |

| Features     | Value |
| --------- | ------------ |
| [Multi Level Epics](https://docs.gitlab.com/ee/user/group/epics/#multi-level-child-epics-ultimate) |  Organize, plan, and prioritize business ideas and initiatives into multi level epics. |

#### Other

| Features    | Value |
| --------- | ------------ |
| [Free Guest Users](https://docs.gitlab.com/ee/user/permissions.html#free-guest-users-ultimate)|  Guest users don’t count towards the license count.  |
| [IP Access Restriction](https://docs.gitlab.com/ee/user/group/#ip-access-restriction-ultimate) |  Enable granular access controls to allow specific people access to specific resources like groups and their underlying projects by IP Address. |


<center><a href="/sales" class="btn cta-btn orange margin-top20">Contact sales and learn more about GitLab Ultimate</a></center>
