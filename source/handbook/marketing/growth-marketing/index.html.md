---
layout: handbook-page-toc
title: Growth Marketing Handbook
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Growth Marketing Overview
{:.no_toc}
---
## Goals

Growth Marketing is responsible for:
* Increasing GitLab.com website traffic 20% YoY on an ongoing basis
* Delivering Inbound MQLs
* Increasing Conversion of free GitLab.com users to paying customers

## Key Deliverables

* GitLab.com public-facing website [view issues board](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website)
* GitLab [blog content](https://about.gitlab.com/blog/)
* GitLab SEO
* GitLab Newsletter

## Communication Overview

### Meeting Cadence

Most of our team meetings are recorded, and can be viewed on GitLab Unfiltered in the [Growth Marketing playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp0T5rN49dxNeJ5uuJ73wMl).

* Monday team leads kickoff to review priorities and blockers
* Friday full team results recap & demo day to share completed work


#### The Handbook 
Is our single source of truth (SSoT) for processes and relevant links

*  Individual teams within Growth Marketing will create their handbook sections
*  The handbook will be iterated on as we establish and test processes

#### When you submit an issue 
You can expect these communications/notifications (either through GitLab or in a comment)

*  You will be provided with specific templates to help you input relevant information
*  Those issues will be vetted at the beginning of each week. The issue will either be: 
   *  Moved into `mktg-status::WIP` or;
   *  A comment will be added for what additional information is needed
*  The issues moved to `mktg-status::WIP` are then put into a sprint and assigned to a week sprint based on priorities, resources, weights and assignment loads
*  The `Assignee` will comment that they have received your Issue and when you can expect more information (ex: "On it. Will respond within the week on timeline")
*  If needed, the Issue Brief will be broken into a Project Epic with relevant issues and those issues re-assigned to the relevant DRIs as `Assignee(s)` 
*  A timeline  will be provided in that Project Epic or individual issue and the due date will be adjusted accordingly.
*  When feedback is needed the `Assignee(s)` will comment in the issue(s) asking for your input. They will be as specific as possible with what they are looking for comments on. 
*  Final deliverables will be added in the appropriate form (file, link to repository, web link, etc.) and the Assignee(s) will @ you
*  When final deliverables have been completed the `Assignee(s)` will close the issue. If additional items are needed please open a new issue and relate to each other.  
*  If an issue needs to be moved to the next milestone the `Assignee` will comment in the issue with the reason (ex: problems with testing)

## Boards
Overarching Marketing Growth Boards

#### [Triage Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1732806?&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Atriage) 

*  **GOAL:** Weed out issues that superfluous or don’t contain extra information 
*  **ACTIONS:** Move to either
   *  Move to either `mktg-status:WIP` which puts into the Sprint cycle or;
   *  [mktg-status: blocked] and comment why it is being blocked

#### [Sprint Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1732820?&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Awip)
*  **GOAL:** Assign issues to milestones to be worked on then based on due dates, weights, priorities, resources
*  **ACTIONS:** 
   *  Move to a specific week taking into consideration the above 
   *  Backlog if:
      *  It doesn’t fit into current goals or priorities
      *  We don't have the time or resources currently
      *  It is just a cool idea (some of these may be labeled for future review for upcoming strategies & tactics) 
   *  Comment why it is backlogged
   *  Never backlog PR issues without prior discussion

   > HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

## Labels
Meanings and usage

*  `mktg-growth`
   *  Denotes it could be part of the Growth Marketing Teams scope. 
   *  Used to help pull in boards
   *  Is not a label to denote the status of an issue
*  `mktg-status::triage`
   *  Universal marketing label
   *  Is used to denote the status of an issue as being reviewed to go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::WIP`
   *  Universal marketing label
   *  Is used to denote the status of an issue as having been reviewed for all details needed and will go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::blocked`
   *  Universal marketing label
   *  Is used to denote the status of an issue as not having enough information to proceed
   *  Does not denote the timeline it will be worked on
*  `mktg-status::review`
   *  Universal marketing label
   *  Is used to denote the status of an issue as in the peer-review process.
   *  Does not denote the timeline it will be worked on
*  [Group Labels] `design`, `content marketing`, `web-analytics`, `mktg-website`, `global content`, `editorial`, `digital-production`
   *  Denotes it could be part of a Growth Marketing Sub-team scope
   *  Is not a label to denote the status of an issue
*  `MG-Support`
   *  Denotes if this is support provided to another team outside of Growth Marketing
   *  Will be used to help quickly view how many issues Growth Marketing supported each year to provide as references for budget and resources requests.  
*  `MG-Review-Future`
   *  Will be reviewed by Marketing Growth for potential future strategy 

## Milestones
Meanings and usage

* `Fri: MONTH DAY`
   * Denotes which week an issue will be worked on 
   * Worked on = will be broken down into smaller Issues or Issue will be completed and closed
* `MG-Backlog:Goal` 
   *  Denotes that this not fit into current goals - can be reviewed later for informing future goals
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:Time/Resources`
   *  Fits into goals, but;
   *  Needs to be reviewed later because we don't have the time or resources during this review
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:General`
   *  Denotes general ideas that are cool ideas but doesn't move the boat forward. Will be reviewed when there is extra time or resources.

## Issue Templates Usage

#### External: 
(outside of Growth Marketing): 

**Issue Author**
* Pick template based on Handbook directions 
  * Template tags will be automatically added:
    * `mktg-growth`
    * `mktg-status::triage`
    * The relevant Group Label: `design`, `content marketing`, `mktg-website`, or `mktg-analytics`
    * `MG-Support` 
  * Template contains specific information to help fill out including directions for picking a due date
  * Template has auto-assignees
  * Links to Handbook page for additional information

#### Internal: 
(Inside of Growth Marketing): 

**Assignees of Issues:**
*  Pick template for breakout of brief
  *  Template tags: 
     *  Keep existing above EXCEPT if we start the project then we remove `MG-Support`
     *  Add your team's process tags
* If the ask (External brief Issue) requires one ask:
  * Re-assign as need, make sure it is in a sprint week and follow documented processes
* If the ask (External brief Issue) requires more than one ask:
   * Create an Epic to hold project
   * Copy and paste markdown into Epic Description and use header: “External Brief”
   * Associate Issue with Epic 
   * Comment Close Issue that the project epic has been created
   * Build out necessary Issues in project, Associate with Epic, assign Issues and update Epic Description as needed with additional brief information 


## Weekly Process

**Beginning of week:** 

*  Geekbot Check-In
* Triage board is vetted and items moved (see above for details on actions)
*  Growth Team Leadership meeting: 
   *  Discuss priorities for the week, Danielle relays exec and PR info, we decide if any project, company or larger Mkrg priorities are bubbling up that would make us reconsider priorities 
   *  Groom issues into weeks using the Sprint Board 

> HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

*  Growth Marketing Leads meets with their teams (anyway they want: meeting, slack, async, geek bot) and relays that information
*  Teams: Assign (if not auto-assigned) Issues, Projects, Tasks and follow their process 


**End of week:**
*  Confirm if everything has been closed and move to next milestone sprint if not completed
*  Comment in issue why it was moved to the next milestone (ex: person x out sick, complications with testing, etc.)

